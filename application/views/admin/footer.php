<footer class="site-footer">
  	<div class="site-footer-legal">© 2018 <a href=""><?php echo config_item('site_name_short') ?></a></div>
  	<div class="site-footer-right">
    	<?php echo config_item('site_name') ?>
  	</div>
</footer>